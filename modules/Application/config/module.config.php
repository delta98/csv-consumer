<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application;

return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'csv-import' => array(
                    'options' => array(
                        'route'    => 'import <importType> [--filePath=] [--exportType=] [--savePath=]',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Application',
                            'action'     => 'process'
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Application' => 'Application\Controller\ApplicationController'
        )
    )
);