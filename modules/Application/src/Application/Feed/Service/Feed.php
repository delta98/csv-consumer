<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Service;

use Application\Feed\Data;
use Application\Feed\Exporter\ExporterInterface;
use Application\Feed\ExportProviderInterface;
use Application\Feed\FeedInterface;
use Application\Feed\Importer\ImportInterface;
use Application\Feed\ImportProviderInterface;

/**
 * Class Feed
 * @package Application\Service
 * @TODO Implement Lazy loading for Importers
 * @TODO Implement Lazy loading for Exporters
 */
class Feed implements FeedInterface, ImportProviderInterface, ExportProviderInterface
{
    /**
     * @var array
     */
    protected $importers = array();

    /**
     * @var array
     */
    protected $exporters = array();

    /**
     * Imports feed from specified type
     *
     * @param $type
     * @param $options
     * @return mixed
     */
    public function import($type, $options = array())
    {
        try {
            // Get Importer from type
            $importer = $this->getImporter($type);

            // Check if setOptions method exists
            if(method_exists($importer, 'setOptions'))
            {
                $importer->setOptions($options);
            }

            // Check import was successful
            if($data = $importer->import())
            {
                return $data;
            }else{
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Exports feed to specified type
     *
     * @param $toType
     * @param Data $data
     * @param array $options
     * @return mixed
     */
    public function export($toType, Data $data, $options = array())
    {
        try {
            // Get Exporter from type
            $exporter = $this->getExporter($toType);

            // Check if setOptions method exists
            if(method_exists($exporter, 'setOptions'))
            {
                $exporter->setOptions($options);
            }

            // Check export was successful
            if($output = $exporter->export($data))
            {
                // Check if a Save Path option was provided
                if(isset($options['save_path']))
                {
                    // Save output to given path
                    try{
                        $result = $this->save($options['save_path'], $output);

                        // Append result to output
                        $output .= $result . "\n";
                    } catch (\Exception $e) {
                        // Append error message to output
                        $output .= $e->getMessage() . "\n";
                    }

                }

                return $output;
            }else{
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Set Importer for provided key
     *
     * @param ImportInterface $importer
     * @param $key
     * @throws \Exception
     */
    public function setImporter(ImportInterface $importer, $key)
    {
        // Check if key already exists
        if(!array_key_exists($key, $this->importers))
        {
            // Set importer to key
            $this->importers[$key] = $importer;
        }else{
            throw new \Exception('Provided Importer for key: ' . $key . ' already exists');
        }
    }

    /**
     * Get Importer for provided key
     *
     * @param $key
     * @return ImportInterface
     * @throws \Exception
     */
    public function getImporter($key)
    {
        // Check if key exists
        if(array_key_exists($key, $this->importers))
        {
            // Return importer for key
            return $this->importers[$key];
        }else{
            throw new \Exception('Importer for key: \'' . $key . '\' doesn\'t exist');
        }
    }

    /**
     * Sets Exporter for provided key
     *
     * @param ExporterInterface $exporter
     * @param $key
     * @throws \Exception
     */
    public function setExporter(ExporterInterface $exporter, $key)
    {
        // Check if key already exists
        if(!array_key_exists($key, $this->exporters))
        {
            // Set exporter to key
            $this->exporters[$key] = $exporter;
        }else{
            throw new \Exception('Provided Exporter for key: ' . $key . ' already exists');
        }
    }

    /**
     * Get Exporter for provided key
     *
     * @param $key
     * @return ExporterInterface
     * @throws \Exception
     */
    public function getExporter($key)
    {
        // Check if key exists
        if(array_key_exists($key, $this->exporters))
        {
            // Return exporter for key
            return $this->exporters[$key];
        }else{
            throw new \Exception('Exporter for key: \'' . $key . '\' doesn\'t exist');
        }
    }

    /**
     * Saves Output to given file path
     *
     * @param $filePath
     * @param $output
     * @return string
     * @throws \Exception
     */
    protected function save($filePath, $output)
    {
        if(@file_put_contents($filePath, $output) === false)
        {
            throw new \Exception('Failed to save to: ' . $filePath);
        }else{
            return "Successfully saved output to: " . $filePath;
        }
    }
}