<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed;

/**
 * Class FeedInterface
 * @package Application\Service
 */
interface FeedInterface
{
    const IMPORT_TYPE_CSV  = 'csv';
    const IMPORT_TYPE_JSON = 'json';
    const EXPORT_TYPE_CSV  = 'csv';
    const EXPORT_TYPE_JSON = 'json';
    const EXPORT_TYPE_XML = 'xml';

    /**
     * Imports feed from specified type
     *
     * @param $type
     * @param $options
     * @return mixed
     */
    public function import($type, $options);

    /**
     * Exports feed to specified type
     *
     * @param $toType
     * @param Data $data
     * @param array $options
     * @return mixed
     */
    public function export($toType, Data $data, $options = array());
}