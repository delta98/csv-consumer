<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed;

use Application\Feed\Exporter\ExporterInterface;

/**
 * Class ExportProviderInterface
 * @package Application\Feed
 */
interface ExportProviderInterface
{
    /**
     * Sets Exporter for provided key
     *
     * @param ExporterInterface $exporter
     * @param $key
     * @throws \Exception
     */
    public function setExporter(ExporterInterface $exporter, $key);

    /**
     * Get Exporter for provided key
     *
     * @param $key
     * @return ExporterInterface
     * @throws \Exception
     */
    public function getExporter($key);
}