<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed;

use \Application\Feed\Importer\ImportInterface;

/**
 * Class ImportProviderInterface
 * @package Application\Service
 */
interface ImportProviderInterface
{
    /**
     * Sets Importer for provided key
     *
     * @param ImportInterface $importer
     * @param $key
     * @throws \Exception
     */
    public function setImporter(ImportInterface $importer, $key);

    /**
     * Get Importer for provided key
     *
     * @param $key
     * @return ImportInterface
     * @throws \Exception
     */
    public function getImporter($key);
}