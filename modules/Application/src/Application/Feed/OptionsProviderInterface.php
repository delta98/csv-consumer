<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed;

/**
 * Class OptionsProviderInterface
 * @package Application\Feed
 */
interface OptionsProviderInterface
{
    /**
     * Set Exporter Options
     *
     * @param array $options
     * @return mixed
     */
    public function setOptions(array $options);

    /**
     * Get Exporter Options
     *
     * @return mixed
     */
    public function getOptions();
}