<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator\InArray;

/**
 * Class Export
 * @package Application\InputFilter
 */
class Export extends InputFilter
{
    /**
     * Constructor
     *
     * @param array $acceptedTypes
     */
    public function __construct($acceptedTypes = array())
    {
        $exportType = new Input('export_type');
        $exportType->getValidatorChain()
                    ->attach(new InArray($acceptedTypes));
    }
}