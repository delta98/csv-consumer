<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator\InArray;

/**
 * Class Import
 * @package Application\InputFilter
 */
class Import extends InputFilter
{
    /**
     * Constructor
     *
     * @param array $acceptedTypes
     */
    public function __construct($acceptedTypes = array())
    {
        $importType = new Input('import_type');
        $importType->getValidatorChain()
                    ->attach(new InArray($acceptedTypes));
    }
}