<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Exporter;

use Application\Feed\Data;

/**
 * Class Csv
 * @package Application\Feed\Exporter
 */
class Csv implements ExporterInterface
{
    /**
     * @var array
     */
    protected $options = array();

    /**
     * @var mixed
     */
    protected $savePath;

    /**
     * Export Data
     *
     * @param \Application\Feed\Data $data
     * @return mixed
     */
    public function export(Data $data)
    {
        // Remove Associate arrays and flatten to single array - as provided to import
        $data = $this->flattenAssociations($data->getData());

        // Generate CSV string for output
        $output = $this->generateCSVOutput($data);

        return $output;
    }

    /**
     * Flatten any associate arrays that were created during import
     *
     * @param $rows
     * @return mixed
     * @TODO use array_walk
     */
    protected function flattenAssociations($rows)
    {
        // Check Data is populated
        if(sizeof($rows) > 0)
        {
            // Loop through each row
            foreach($rows as $rowNum => $row)
            {
                // Loop through each column within the row
                foreach($row as $columnHeader => $column)
                {
                    // Check if column should be flattened
                    if(is_array($column))
                    {
                        // Get column headers from array keys
                        $keys = array_keys($column);

                        // Loop through each column header (key) and create a flat index of it's data
                        foreach($keys as $key)
                        {
                            $rows[$rowNum][$columnHeader . '_' . $key] = $column[$key];
                        }

                        // Remove old Associated Index
                        unset($rows[$rowNum][$columnHeader]);
                    }
                }
            }
        }

        return $rows;
    }

    /**
     * Generate CSV string from Rows
     *
     * @param $rows
     * @param string $separator
     * @param string $lineBreak
     * @return string
     */
    protected function generateCSVOutput($rows, $separator = ',', $lineBreak = "\n")
    {
        // Declare function params
        $formattedString = "";
        $setColumnHeaders = false;

        // Loop through each row and format to csv
        foreach($rows as $row)
        {
            // Check if column headers have been set
            if(!$setColumnHeaders)
            {
                // Get column headers from row index keys
                $header = implode($separator, array_keys($row)) . $lineBreak;
                $formattedString .= $header;
                $setColumnHeaders = true;
            }

            // Format  column data for row
            $row = implode($separator, $row) . $lineBreak;

            // Append to formatted string
            $formattedString .= $row;
        }

        return $formattedString;
    }
}