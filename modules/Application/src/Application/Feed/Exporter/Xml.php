<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Exporter;

use Application\Feed\Data;

/**
 * Class Xml
 * @package Application\Feed\Exporter
 */
class Xml implements ExporterInterface
{
    /**
     * XML element
     *
     * @var string
     */
    protected $element = '<feed/>';

    /**
     * @var \SimpleXMLElement
     */
    protected $simpleXMLElement;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->simpleXMLElement = new \SimpleXMLElement($this->getElement());
    }

    /**
     * Export Data
     *
     * @param \Application\Feed\Data $data
     * @return mixed
     */
    public function export(Data $data)
    {
        // Get Data from Feed Data object
        $nodes = $data->getData();

        $items = $this->simpleXMLElement->addChild('items');

        // Create an XML element for each node
        foreach($nodes as $node)
        {
            $item = $items->addChild('item');

            // Walk through each node and create an XML Element
            array_walk($node, array($this, 'createXMLElement'), $item);
        }

        // Get XML Output
        $output = $this->simpleXMLElement->asXML();

        return $output;
    }

    /**
     * Creates XML Elements for provided $value and $key pairs
     *
     * @param $value
     * @param $key
     * @param $item
     */
    public function createXMLElement($value, $key, $item)
    {
        // Check if value is an array
        if(!is_array($value))
        {
            // Create XML child element
            $item->addChild($key, $value);
        }else{
            // Create Parent XML element for children
            $parent = $this->simpleXMLElement->addChild($key);

            // Get children
            $children = array_keys($value);

            // Create a sub child XML element for each of the parent elements children
            foreach($children as $child)
            {
                $parent->addChild($child, $value[$child]);
            }
        }
    }

    /**
     * @param string $element
     */
    public function setElement($element)
    {
        $this->element = $element;
    }

    /**
     * @return string
     */
    public function getElement()
    {
        return $this->element;
    }
}