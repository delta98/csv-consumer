<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Exporter;

use Application\Feed\Data;

/**
 * Class ExporterInterface
 * @package Application\Feed\Exporter
 */
interface ExporterInterface
{
    /**
     * Export Data
     *
     * @param \Application\Feed\Data $data
     * @return mixed
     */
    public function export(Data $data);
}