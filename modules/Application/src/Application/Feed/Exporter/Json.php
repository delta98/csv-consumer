<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Exporter;

use Application\Feed\Data;
use Zend\Serializer\Adapter\Json as JsonSerializerAdapter;

/**
 * Class Json
 * @package Application\Feed\Exporter
 */
class Json implements ExporterInterface
{
    /**
     * Export Data
     *
     * @param \Application\Feed\Data $data
     * @return mixed
     */
    public function export(Data $data)
    {
        // Init Json Serializer
        $jsonSerializer = new JsonSerializerAdapter();

        try {
            // Serialize Data to JSON
            $output = $jsonSerializer->serialize($data->getData());

            return $output;
        } catch (\Exception $e) {
            return false;
        }
    }
}