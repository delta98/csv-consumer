<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Importer;

/**
 * Class ImportInterface
 * @package Application\Service
 */
interface ImportInterface
{
    /**
     * Import Data
     *
     * @return \Application\Feed\Data
     */
    public function import();
}