<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Feed\Importer;

use Application\Feed\Data;
use Application\Feed\OptionsProviderInterface;
use Zend\Serializer\Adapter\Json as JsonSerializerAdapter;

/**
 * Class Json
 * @package Application\Feed\Exporter
 */
class Json implements ImportInterface, OptionsProviderInterface
{
    /**
     * @var array
     */
    protected $options = array();

    /**
     * @var mixed
     */
    protected $data;

    /**
     * Import Data
     *
     * @return bool|\Application\Feed\Data
     */
    public function import()
    {
        // Get JSON Serializer Adapter
        $jsonSerializerAdapter = new JsonSerializerAdapter();

        try{
            // Unserialize JSON data
            $data = $jsonSerializerAdapter->unserialize($this->getData());

            // Check a valid array was returned
            if(is_array($data))
            {
                // Return Feed Data
                return new Data($data);
            }else{
                // Error occurred with JSON decoding
                return false;
            }

        } catch(\Exception $e) {
            return false;
        }

    }

    /**
     * Set Exporter Options
     *
     * @param array $options
     * @return mixed|void
     * @throws \Exception
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        // Check if JSON Data has been provided
        if(isset($this->options['json_data']))
        {
            $this->setData($this->options['json_data']);
        }

        // Check if a file path has been provided to capture JSON data from
        if(isset($this->options['file_path']))
        {
            $this->captureDataFromFile($this->options['file_path']);
        }
    }

    /**
     * Get Exporter Options
     *
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Capture JSON data from provided file
     *
     * @param $filePath
     * @return bool
     */
    protected function captureDataFromFile($filePath)
    {
        // Get contents from file
        $contents = @file_get_contents($filePath);
        if($contents !== false)
        {
            // Check contents are valid JSON
            if(json_decode($contents) !== null)
            {
                $this->setData($contents);

                return true;
            }
        }

        return false;
    }

}