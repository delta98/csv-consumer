<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application\Controller;

use Application\Feed\Data;
use Application\Feed\FeedInterface;
use Application\Feed\InputFilter\Export;
use Application\Feed\InputFilter\Import;
use Application\Feed\Service\Feed;
use Zend\Console\Request as ConsoleRequest;
use Zend\Console\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ConsoleModel;

/**
 * Class ApplicationController
 * @package Application\Controller
 */
class ApplicationController extends AbstractActionController
{
    /**
     * Processes input from the command line
     *
     * @return bool|mixed|string
     * @throws \RuntimeException
     */
    public function processAction()
    {
        // Get Console Request
        $request = $this->getRequest();

        // Make sure we've received a console request
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('Oi Cheeky, this is a console application');
        }

        // Get Feed service from Service Manager
        $feedService = $this->getServiceLocator()->get('Application\Feed\Service');

        // Receive input from CLI
        if(!$feedData = $this->inputData($feedService, $request))
        {
            return "Import failed \n";
        }

        // Output to CLI
        if($output = $this->outputData($feedData, $feedService, $request))
        {
            return $output;
        }
    }

    /**
     * Input Data
     *
     * @param Feed $feedService
     * @param Request $request
     * @return bool|Data
     */
    protected function inputData(Feed $feedService, Request $request)
    {
        // Import options
        $options = array();

        // Check if filePath Flag was used
        if($filePath =  $request->getParam('filePath'))
        {
            $options['file_path'] = $filePath;
        }

        // Define allowed Import Types
        $importInputFilter = new Import(array(
            FeedInterface::IMPORT_TYPE_CSV,
            FeedInterface::IMPORT_TYPE_JSON,
        ));

        // Get Import Type from params
        $importType = $request->getParam('importType');

        // Set Import type to be validated against
        $importInputFilter->setData(array('import_type' => $importType));

        // Check for Valid exporter
        if($importInputFilter->isValid())
        {
            $feedData = $feedService->import($importType, $options);

            return $feedData;
        }else{
            return false;
        }
    }

    /**
     * Output Data
     *
     * @param Data $feedData
     * @param Feed $feedService
     * @param Request $request
     * @return bool|mixed
     */
    protected function outputData(Data $feedData, Feed $feedService, Request $request)
    {
        // Check for valid Feed Data
        if($feedData instanceof Data)
        {
            // Check if ExportType Flag has been set
            if($exportType = $request->getParam('exportType'))
            {
                // Export options
                $options = array();

                // Check if savePath Flag was used
                if($savePath = $request->getParam('savePath'))
                {
                    $options['save_path'] = $savePath;
                }

                // Define allowed Export Types
                $exportInputFilter = new Export(array(
                    FeedInterface::EXPORT_TYPE_CSV,
                    FeedInterface::EXPORT_TYPE_JSON,
                    FeedInterface::EXPORT_TYPE_XML
                ));

                // Set Export type to be validated against
                $exportInputFilter->setData(array('export_type' => $exportType));

                // Check for Valid exporter
                if($exportInputFilter->isValid())
                {
                    return $feedService->export($exportType, $feedData, $options);
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }
}