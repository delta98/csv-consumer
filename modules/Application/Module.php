<?php
 
 /**
 * CSV Consumer Application
 * 
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace Application;

use Application\Feed\FeedInterface;
use Application\Feed\Importer\Csv as CsvImporter;
use Application\Feed\Importer\Json as JsonImporter;
use Application\Feed\Exporter\Csv as CsvExporter;
use Application\Feed\Exporter\Json as JsonExporter;
use Application\Feed\Exporter\Xml as XmlExporter;
use Application\Feed\Service\Feed;
use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Class Module
 * @package Application
 */
class Module implements ServiceProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface,
                        ConsoleBannerProviderInterface, ConsoleUsageProviderInterface
{
    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Feed\Service' => function() {
                    $feedService = new Feed();

                    // Set Importers
                    $feedService->setImporter(new CsvImporter(), FeedInterface::IMPORT_TYPE_CSV);
                    $feedService->setImporter(new JsonImporter(), FeedInterface::IMPORT_TYPE_JSON);

                    // Set Exporters
                    $feedService->setExporter(new CsvExporter(), FeedInterface::EXPORT_TYPE_CSV);
                    $feedService->setExporter(new JsonExporter(), FeedInterface::EXPORT_TYPE_JSON);
                    $feedService->setExporter(new XmlExporter(), FeedInterface::EXPORT_TYPE_XML);

                    return $feedService;
                },
            )
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Returns a string containing a banner text, that describes the module and/or the application.
     * The banner is shown in the console window, when the user supplies invalid command-line parameters or invokes
     * the application with no parameters.
     *
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access Console and send
     * output.
     *
     * @param AdapterInterface $console
     * @return string|null
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return
            "==------------------------------------------------------==\n" .
            "                     Feed Import/Export                   \n" .
            "==------------------------------------------------------==\n"
            ;
    }

    /**
     * Returns an array or a string containing usage information for this module's Console commands.
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access
     * Console and send output.
     *
     * If the result is a string it will be shown directly in the console window.
     * If the result is an array, its contents will be formatted to console window width. The array must
     * have the following format:
     *
     *     return array(
     *                'Usage information line that should be shown as-is',
     *                'Another line of usage info',
     *
     *                '--parameter'        =>   'A short description of that parameter',
     *                '-another-parameter' =>   'A short description of another parameter',
     *                ...
     *            )
     *
     * @param AdapterInterface $console
     * @return array|string|null
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'import <importType>' => 'Import data of a specified type, currently supports csv and json',
            '--filePath='         => 'Optional Flag to specify a file to read data from',
            '--exportType='       => 'Optional Flag to specify the format to export to, currently supports csv, json and xml',
            '--savePath='         => 'Optional Flag to specify a file path to save the exported data to',
            'Example: import csv --filePath=\location\to\file.csv --exportType=xml',
            'Example: import json --filePath=\location\to\file.json --exportType=csv --savePath=\location\to\save\file.csv',
        );
    }
}