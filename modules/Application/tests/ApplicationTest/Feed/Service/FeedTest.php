<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace ApplicationTest\Service;

use Application\Feed\Data;
use Application\Feed\Service\Feed;

/**
 * Class Feed
 * @package ApplicationTest\Service
 */
class FeedTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Application\Feed\Service\Feed
     */
    protected $feedService;

    /**
     * Set Up Tests
     */
    public function setUp()
    {
        $this->feedService = new Feed();
    }

    public function tearDown()
    {
        $this->feedService = null;
    }

    public function testImportSuccessful()
    {
        $csvImporter = $this->getMock('\Application\Feed\Importer\Csv', array('import'));
        $csvImporter->expects($this->any())
                    ->method('import')
                    ->will($this->returnValue(true));

        $this->feedService->setImporter($csvImporter, 'csv');

        $this->assertTrue($this->feedService->import('csv', array('file_path' => '')));
    }

    public function testImportFailed()
    {
        $csvImporter = $this->getMock('\Application\Feed\Importer\Csv', array('import'));
        $csvImporter->expects($this->any())
            ->method('import')
            ->will($this->returnValue(false));

        $this->feedService->setImporter($csvImporter, 'csv');

        $this->assertFalse($this->feedService->import('csv', array('file_path' => '')));
    }

    public function testImportFailedDueToInvalidOptions()
    {
        $csvImporter = $this->getMock('\Application\Feed\Importer\Csv', array('import'));
        $csvImporter->expects($this->any())
            ->method('import')
            ->will($this->returnValue(true));

        $this->feedService->setImporter($csvImporter, 'csv');

        $this->assertFalse($this->feedService->import('csv'));
    }

    public function testImportFailedDueToInvalidImporterKey()
    {
        $this->assertFalse($this->feedService->import('csv'));
    }

    public function testExceptionDueToDuplicateImporterKey()
    {
        $this->setExpectedException('\Exception');

        $csvImporter = $this->getMock('\Application\Feed\Importer\Csv', array('import'));
        $csvImporter->expects($this->any())
            ->method('import')
            ->will($this->returnValue(true));

        $this->feedService->setImporter($csvImporter, 'csv');
        $this->feedService->setImporter($csvImporter, 'csv');
    }

    public function testExportSuccessful()
    {
        $csvExporter = $this->getMock('\Application\Feed\Exporter\Csv', array('export'));
        $csvExporter->expects($this->any())
            ->method('export')
            ->will($this->returnValue(true));

        $this->feedService->setExporter($csvExporter, 'csv');

        $this->assertTrue($this->feedService->export('csv', new Data(
            array(
                array(
                    'name' => 'Bob',
                    'address' => array(
                        1 => 'A street',
                        2 => 'A town',
                        'postcode' => 'AA1AA'
                    )
                )
            )
        )));
    }

    public function testExportFailed()
    {
        $csvExporter = $this->getMock('\Application\Feed\Exporter\Csv', array('export'));
        $csvExporter->expects($this->any())
            ->method('export')
            ->will($this->returnValue(false));

        $this->feedService->setExporter($csvExporter, 'csv');

        $this->assertFalse($this->feedService->export('fail', new Data(
            array(
                array(
                    'name' => 'Bob',
                    'address' => array(
                        1 => 'A street',
                        2 => 'A town',
                        'postcode' => 'AA1AA'
                    )
                )
            )
        )));
    }
}