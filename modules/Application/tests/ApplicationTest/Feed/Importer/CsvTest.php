<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace ApplicationTest\Service\Importer;

use Application\Feed\Importer\Csv;

/**
 * Class CsvTest
 * @package ApplicationTest\Service\Importer
 */
class CsvTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Application\Feed\Importer\Csv
     */
    protected $csvImporter;

    public function setUp()
    {
        $this->csvImporter = new Csv();
    }

    public function tearDown()
    {
        $this->csvImporter = null;
    }

    public function testImportSuccess()
    {
        $this->csvImporter->setFilePath(__DIR__ . '/../../../data/test.csv');
        $this->assertInstanceOf('\Application\Feed\Data', $this->csvImporter->import());
    }

    public function testImportSuccessWithOptions()
    {
        $this->csvImporter->setOptions(
            array(
               'file_path' => __DIR__ . '/../../../data/test.csv'
            )
        );

        $this->assertInstanceOf('\Application\Feed\Data', $this->csvImporter->import());
    }

    public function testImportFailedUnableToOpenFile()
    {
        $this->assertFalse(@$this->csvImporter->import());
    }
}