<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace ApplicationTest\Service\Exporter;

use Application\Feed\Data;
use Application\Feed\Exporter\Csv;

/**
 * Class CsvTest
 * @package ApplicationTest\Service\Importer
 */
class CsvTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Application\Feed\Exporter\Csv
     */
    protected $csvExporter;

    public function setUp()
    {
        $this->csvExporter = new Csv();
    }

    public function tearDown()
    {
        $this->csvExporter = null;
    }

    public function testExportSuccess()
    {
        $output = $this->csvExporter->export(
            new Data(
                array(
                    array(
                        'name' => 'Bob',
                        'address' => array(
                            1 => 'A street',
                            2 => 'A town',
                            'postcode' => 'AA1AA'
                        )
                    )
                )
            )
        );

        $this->assertTrue(!is_null($output));
    }
}