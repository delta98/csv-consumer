<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace ApplicationTest\Service\Exporter;

use Application\Feed\Data;
use Application\Feed\Exporter\Xml;

/**
 * Class XmlTest
 * @package ApplicationTest\Service\Exporter
 */
class XmlTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Application\Feed\Exporter\Xml
     */
    protected $xmlExporter;

    public function setUp()
    {
        $this->xmlExporter = new Xml();
    }

    public function tearDown()
    {
        $this->xmlExporter = null;
    }

    public function testExportSuccess()
    {
        $output = $this->xmlExporter->export(
            new Data(
                array(
                    array(
                        'name' => 'Bob',
                        'address' => array(
                            1 => 'A street',
                            2 => 'A town',
                            'postcode' => 'AA1AA'
                        )
                    )
                )
            )
        );

        $this->assertFalse(is_null($output));
    }
}