<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

namespace ApplicationTest\Service\Exporter;
use Application\Feed\Data;
use Application\Feed\Exporter\Json;

/**
 * Class JsonTest
 * @package ApplicationTest\Service\Exporter
 */
class JsonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Application\Feed\Exporter\Json
     */
    protected $jsonExporter;

    public function setUp()
    {
        $this->jsonExporter = new Json();
    }

    public function tearDown()
    {
        $this->jsonExporter = null;
    }

    public function testExportSuccess()
    {
        $output = $this->jsonExporter->export(
            new Data(
                array(
                    array(
                        'name' => 'Bob',
                        'address' => array(
                            1 => 'A street',
                            2 => 'A town',
                            'postcode' => 'AA1AA'
                        )
                    )
                )
            )
        );

        $this->assertTrue(is_array(json_decode($output)));
    }
}