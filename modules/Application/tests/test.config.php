<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

// Return Application Config - ensure test.local.php has been setup
return include __DIR__ . '/../../../config/application.config.php';