<?php

/**
 * CSV Consumer Application
 *
 * @author Jason Brown <jason.brown.delta@gmail.com>
 */

return array(
    'modules' => array(
        'ZendDeveloperTools',
        'Application'
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            'config/autoload/' . (getenv('APPLICATION_ENV')? getenv('APPLICATION_ENV') . '.' : '*') . '{global,local}.php',
        ),
        'config_cache_enabled' => false,
        'cache_dir'            => 'data/cache',
        'module_paths' => array(
            './modules',
            './vendor',
        ),
    ),
);
